# Preparing Seneca's first letter to Lucilius for reading

+ get the text
+ clean up the text

+ tokenize into sentences (tokenize-text-file.py)
+ transform into simple XML  (json-to-p-s-with-numbering.xq)
+ tokenize sentences into words (tokenize-xml-sentences-into-words-and-pc.xq)
+ lemmatize words using LEMLAT (call-lemlat.xq)

  + correct LEMLAT's XML replacing double quotes with HTML entities: regex replace `(lemma="[^"]+)"([^ "]+")` with `$1&quot;$2`

+ transform results into an easily analyzable TEI fragment (readLEMLATxml-multiple-lem-diff.xq):

  + the unambiguous lemmata are ready for creating exercises
  + any of the multiple derivational candidates (turpissima < turpissimus / turpis, `@ana="deriv"`) can be used in exercises
  + manually assign an estimated certainty degree (`cert="50"`) to multiple lemmata candidates (`@ana="multilem"`, for example seruo, serua, seruus)

+ analyze vocabulary by frequency: 
  + local in the letter
  + local in Seneca's letters
  + local in all Seneca
  + general in Latin
  
+ add translations to vocabulary
  + use existing dictionaries / lexica

+ prepare exercises for vocabulary

   + most frequent vocabulary in passage (get-sentences-by-order.xq - words with DefHR element): vocabulary learning exercises
   + any other lemmatized words in passage: show meanings first, then show exercises
   + exercise with probabilities

+ prepare exercises for sentences (elicited response)

   + select shorter sentences
   + fill in the gaps
   + mix translated words with original ones
   + show literal translation of lemmata

+ import exercises into Moodle

+ segment sentences into phrases
+ add translations
