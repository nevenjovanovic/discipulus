# Practising Ancient Greek and Latin with a set of simple sentences

## Components

+ a corpus of simple sentences (preferably from original Greek and Latin texts, or transformed only slightly - 3rd person into 2nd, singular into plural)
+ a set of words / grammatical features we want to practise
+ an active recall testing and spaced repetition system (such as [Anki](https://apps.ankiweb.net/docs/manual.html))
+ a large set of exercises (words in sentences)

## Exercises

### Basic

In my opinion, the basic exercise type is at the same time most efficient (and hardest), as it requires most concentration.

See sentence, produce the translation, check the answer, rate your success (the system takes care of when you will see the same exercise / sentence again).

The same, but from the translation to the original sentence (harder).

Support: audio.

### Variations / use for review

*Elicited repetition*: hear the sentence, write it down. Read the sentence, write it down. Hear the sentence, record the audio, compare it yourself, rate it.

*Fill in the gaps*. After learning the sentence, see it with a piece missing; supply what's missing.

*Reconstruct*. After learning the sentence, manipulate the jumbled words into correct order

*Change form*. Recognize the same sentence in lowercase / uppercase, manuscript, old print.

