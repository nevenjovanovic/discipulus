# Preparing a reading corpus

What do we want to know, what do we need (ideally):

+ the texts, tokenized into chapters, sentences, phrases, and words
+ frequencies for the whole corpus, for texts, chapters (and even sentences)
+ lemmas of words
+ frequencies for the lemmatized corpus, texts, chapters, sentences
+ translations

# Case study: reading list for the "Translation from Latin" course at the University of Zagreb

+ Seneca, Letters to Lucilius 1
+ Terence, Adelphoe
+ Horace, Odes 1
+ Tibullus 1

# Compile the corpus

Get the texts from Perseus DL:

+ Epistulae Morales ad Lucilium 1 [urn:cts:latinLit:phi1017.phi015](http://catalog.perseus.org/catalog/urn:cts:latinLit:phi1017.phi015)
+  Adelphoe [urn:cts:latinLit:phi0134.phi006](http://data.perseus.org/catalog/urn:cts:latinLit:phi0134.phi006)
+ Odae 1 [urn:cts:latinLit:phi0893.phi001](http://data.perseus.org/catalog/urn:cts:latinLit:phi0893.phi001)
+ Tibullus, Carmina 1 [urn:cts:latinLit:phi0660.phi001](http://data.perseus.org/catalog/urn:cts:latinLit:phi0660.phi001)

The task is not quite trivial, because we need just some segments (some books) of these works, and also -- as it turns out -- because the texts contain critical notes and, in two places, funny division of words, because of Horatian metres.

# Get word list and frequencies

It turns out that the course corpus contains 24,318 words. Which, accidentally, means that a student should, in 120 days of our winter semester, prepare somewhat more than 200 words each day -- not counting the reviewing.

The corpus by texts:

+ Seneca: 6272
+ Terence: 8874
+ Horace: 3967
+ Tibullus: 5211

# Lemmatize the corpus

The word list can be fed to LEMLAT for lemmatization analysis. The results:

+ Number of word forms: 8548
+ Number of forms unknown to the program: 103
+ Number of forms analysed: 8445

## Annotate LEMLAT analyses for single or multiple candidates

According to an earlier experiment, we want to distinguish forms with multiple candidate lemmata (multilem) from the forms with unambiguous lemmata (both derived and unique).

There is a script for that: [corpus-LEMLAT-xml-into-TEI-fragment.xq]().

The resulting TEI XML fragment can be used to annotate forms with analyses pointing to a single lemma.

Some LEMLAT lemmata are homonyms, or even identical, though they have different LEMLAT id numbers; if we isolate such cases, the number of annotated forms can be enlarged further.

## Tokenize the texts into sentences and words

Now is the time to tokenize our corpus into sentences and words. For sentences, CLTK has the best tool, so we want to use that as a pair of Python scripts -- one to read all text files, the other to tokenize the text in them into sentences. The script reading the list of files in the directory is also calling the tokenizing script.

The first script is [iterate-over-files.py](). It calls the tokenizing script [tokTextFile.py]().

First we need text-only versions of our segments. The best approach -- because we also want to take note of chapters and sub-chapters to which the sentences belong -- is to prepare one text file for each basic `div` segment ("basic" in the sense that it contains only `p` or `l` elements, no other `div`s). This is done by the XQuery [transform-segments-to-plaintext.xq](). 

The Python CLTK function outputs its results as JSON, which we move into a separate `/json` directory.

Then we will want to reconstruct from JSON files the text documents, with all their chapters, scenes, letters, and poems, but also with sentences below these levels, and tokenized into words and punctuation as well. For that, we have two XQuery scripts. The first annotates punctuation: [corpus-tokenize-punctuation.xq]() (it is not perfect -- at the moment it does not distinguish between sentence punctuation and parts of abbreviation). The second tokenizes text nodes of documents produced by the first -- these nodes now contain only words and spaces -- and wraps each word into a `w` element.

## Annotate word forms in corpus with pointers to LEMLAT lemmata

Now we can start adding LEMLAT lemmata and lemma references to `w` elements, in the format `<w lemma="homo" lemmaRef="lemlat:h234">homo</w>`.

First, we will make an XML database using our LEMLAT TEI fragment, [corpus-create-LEMLAT-TEI-db.xq]() and another database, `psl-sent` of sections tokenized into sentences and words [corpus-create-sentences-words-db.xq]().

Then we will read unambiguous lemma identifications and add them to words in the corpus: [corpus-insert-unamb-lemref-into-w.xq](). This is not a fastest operation, it takes 89778.5 ms, but it annotates 10,412 of our 24,268 word tokens -- some 43%.

We can also annotate words with multiple lemmata, but derived from the same basic lemma: [corpus-insert-lemref-deriv-into-w.xq](). In 23337.3 ms, the number of annotated w tokens has grown to 11,749. Now a little bit over 48% of words in the corpus is lemmatized.

Lemmatizing the rest will require a change of tactics. We can start from the frequencies of unlemmatized forms, or from an alphabetical concordance.

# Create exercises

There are several approaches to creating exercises too. For some (practising vocabulary and sentences), we will use Anki; for others (adding translations), we can use the Database activity in Moodle.

The basic one, however, seems to be: teach the students to use a concordance program (and, later, Anki) so they can create their own exercises.

## Create database for translating sentences

The [Moodle Database Activity](https://docs.moodle.org/33/en/Database_activity) module is ideal for building a collection of translated sentences. The sentences can be prepared as a coherent set, with a certain word appearing in each of them. Each student can work on a prescribed number of sentences per week; each may be assigned a different set; each translation can be approved (or disapproved) by the teacher; the activity can be graded. This activity stimulates continuous, easy to control engagement with sentences from the reading list during the semester.

Under a very mild homework regime -- five sentences per week, say -- each student will have translated 75 sentences by the end of the semester (the whole corpus has some 2,650 sentences).

## Create cards for Anki

### Some calculations

If we study 5 new cards each day -- 35 cards a week -- 525 cards in 105 days of our Winter Semester. That would mean being immersed in, or intensively exposed to, some 2,600 words (if the average is five words on a card). That is 10% of the reading list. 20 new cards a day (demanding, but not impossible) would expose us to 10,500 words -- 40% of the corpus.

### Interesting exercise types

+ Latin word / Croatian meaning and reverse (to introduce a word)
+ Sentence with a Croatian translation, and reverse
+ Cloze card with the word to be supplied in Croatian (in its vocabulary form)
+ Smaller, meaningful phrases from sentences with translation and clozes
+ Croatian words in vocabulary form, ordered as in the sentence (produce the correct sentence in Latin!)

### Procedure

Search by LEMLAT lemma ID, or in concordance. Choose words that the students may not know, with several occurrences in the corpus. Produce a rough version of exercises automatically, edit manually.



