(: Return lemmata and Croatian meanings, for proofreading :)

for $g in db:open("greeklatincore","greek_core_list_2.xml")/csv/record/DefHR
order by $g collation "?lang=hr-HR"
return element a { $g/../Lemma , $g }