for $r in db:open("greeklatincore","latin_core_list_2.xml")//record
where matches($r/POSHR, "Imenica")
order by xs:int($r/Frequency-Rank/string())
return $r/Lemma