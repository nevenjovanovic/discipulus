(: Pull from PROIEL's Github repo :)
let $path := doc('https://github.com/proiel/proiel-treebank/raw/master/latin-nt.xml') 
return db:create("lat_nt_tb", $path, (), map { 'intparse' : true() })