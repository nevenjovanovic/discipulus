declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=lat&amp;engine=morpheuslat&amp;word=" || $w
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
let $send :=
let $words :=
distinct-values(for $w in db:open("lat_simpl_tok","simpl.xml")/body/cit/quote/s/w
return lower-case($w))
for $w in $words
order by $w
return $w
for $w in $send
let $parse := local:pers_morph($w)//*:RDF//*:Body
where not($parse[2])
return element w { attribute form {$w} , $parse//entry }