declare function local:down_tree($root){
  let $id := $root/@id
  let $down_t := $root/../*:word[@head=$id]
  return if ($down_t[@head]) then ( $down_t/@form/string() , local:down_tree($down_t) )
  else $down_t/@form/string()
};
for $phr in
for $prep in db:open("pdl_tb_lat","phi0474.phi013.perseus-lat1.tb.xml")/treebank/body/sentence/word[@postag="r--------"]
return element phr { $prep/@form/string() , local:down_tree($prep) }
order by $phr
return element phr { for $w in tokenize($phr, ' ') return element w { $w } }