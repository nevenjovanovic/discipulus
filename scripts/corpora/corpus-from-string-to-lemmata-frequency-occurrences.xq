(: given a string / word form beginning, search for lemmata and for occurrences in the corpus :)

declare function local:count_occurrences($form){
  count(collection("psl-sent")//s/w[lower-case(.)=$form])
};

declare function local:count_occurrences_lemma($lemma){
  count(collection("psl-sent")//s/w[@lemmaRef=$lemma])
};

declare function local:table($result , $link){
  element tr {
  element td { $result },
  element td { $link }
}
};
declare function local:search_lemlat($lemlat){
  let $lemma := distinct-values(collection("psl-lemlat")//*:interp[@n=$lemlat]/@ana)
  return $lemma
};
declare function local:return_lemlat_or_string($string){
let $search := $string || ".*"
for $occur in collection("psl-sent")//s/w[matches(text(), $search)]
return if ($occur/@lemmaRef) then $occur/@lemmaRef/string()
else $occur/string()  
};
let $string := "rap"
let $result := local:return_lemlat_or_string($string)
let $values := distinct-values($result)
for $v in $values
return if (starts-with($v, "lemlat:")) then 
 local:table($v, ( local:search_lemlat($v), local:count_occurrences_lemma($v)
))
else 
 local:table($v, local:count_occurrences($v))