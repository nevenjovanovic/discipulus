let $path := replace(file:parent(static-base-uri()), '/scripts/corpora/', '/psl-corpus/')
let $p := $path || "psl-LEMLAT-TEI-fragment.xml"
return db:create("psl-lemlat", $p, (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() })