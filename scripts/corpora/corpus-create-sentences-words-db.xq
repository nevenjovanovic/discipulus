(: create a db of TEI fragments, div segments tokenized into sentences and words :)
let $path := replace(file:parent(static-base-uri()), '/scripts/corpora/', '/psl-corpus/xmlsentences/')
let $p := $path
return db:create("psl-sent", $p, (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() })