(: with a selection of text segments (corrected) in data directory, create a db :)
let $psl := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/data"
return db:create("psl-corpus-2017", $psl, (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() })