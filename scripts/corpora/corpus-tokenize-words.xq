(: Tokenize text nodes into words :)
(: separate by a series of non-word characters :)

let $dir := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/xmlsentences/"
let $files := file:list($dir)
for $f in $files return
copy $xml := fetch:xml($dir || $f)
modify ( for $s in $xml//s/text()
let $words := tokenize(normalize-space($s), "\W+")
return replace node $s with
for $w in $words return element w {
  $w
}
)
return file:write($dir || $f , $xml)