let $s := //*:w[not(@lemmaRef) and string()="istane"]
let $corr := element w {
  attribute lemmaRef { "lemlat:i2947" } ,
  "istanc"
}
return replace node $s with $corr