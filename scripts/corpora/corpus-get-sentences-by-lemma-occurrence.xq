(: find lemmata with n occurrences :)
(: show their sentence contexts :)
(: eventually, create a set of Anki notes :)
declare function local:prepare_text($s){
  replace(
  replace(lower-case(normalize-space($s)), " (\W)", "$1"),
  '"', '')
};

declare function local:list_lemmata_by_freq($freq){
  for $l in  collection("psl-sent")/div/s/w[@lemmaRef]
let $lref := $l/@lemmaRef
group by $lref
where count($l) >= $freq and count($l) < 10
return $lref
};
let $csv := element csv {
let $lemmata := local:list_lemmata_by_freq(3)
for $lem in $lemmata
let $sentences :=  collection("psl-sent")/div/s[w/@lemmaRef=$lem]
for $s in $sentences
let $chap_id := $s/../@xml:id
let $sent_id := $chap_id || "." || $s/@n
let $text := local:prepare_text($s)
return element record {
  element id { $sent_id } ,
  element txt { $text } ,
  element hr { "TRL" },
  element lemma { $lem }
}
}
return csv:serialize($csv, map { 'separator': 'tab' })