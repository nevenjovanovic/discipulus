declare variable $corpus := (
  "phi1017.phi015.perseus-lat2",
  "phi0134.phi006.perseus-lat2",
  "phi0893.phi001.perseus-lat2",
  "phi0660.phi001.perseus-lat2"
);
let $ter := for $c in $corpus[2]
return element body {
  attribute xml:base { $c }, collection($c)//*:TEI/*:text/*:body/*:div/*:div[position()>1]
}
let $ceteri := for $c in $corpus[position() != 2]
return element body {
  attribute xml:base { $c },
  collection($c)//*:TEI/*:text/*:body/*:div/*:div[1]
}
return db:create("psl-corpus-2017", ( $ter, $ceteri ), (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() })