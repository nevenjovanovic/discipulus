(: count lemmatized words in each text :)
declare variable $urn := ("phi1017.phi015", "phi0134.phi006", "phi0660.phi001", "phi0893.phi001");
for $u in $urn
let $ff := db:open("psl-sent")/*:div[starts-with(@xml:id, $u)]//*:w[@lemmaRef]
let $ff2 := db:open("psl-sent")/*:div[starts-with(@xml:id, $u)]//*:w
return element l { 
attribute id { $u },
count($ff2) , ":" , count($ff) }