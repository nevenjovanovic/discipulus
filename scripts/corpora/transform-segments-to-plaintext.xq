(: tokenize texts into sentences, then words :)
(: start from plain text versions :)

declare function local:make_text_nodes($nodes){
  normalize-space(
    string-join($nodes, ' '))
};

(: create a CTS-like identifier for segments :)
(: we'll use that as JSON file name :)
declare function local:create_name($t){
  let $urn := $t/ancestor::body/@xml:base
let $up := $t/ancestor::*:div/@n
let $this := $t/@n
let $id := string-join(($urn, $up, $this), ".")
return $id
};

(: retrieve all divs which have no div children :)
let $text_no_notes := for $t in collection("psl-corpus-2017")/body//*:div[not(*:div)]
let $id := local:create_name($t)
let $path := replace(file:parent(static-base-uri()), '/scripts/corpora/', '/psl-corpus/plaintext/')
(: we don't need the contents of notes :)
let $nodes := $t//text()[not(ancestor::*:note)]
let $txt := local:make_text_nodes($nodes)
let $filename := $path || $id || ".txt"
return element result { element name { $filename } , element text { $txt } }
return for $t in element psl { $text_no_notes }//result
let $name := $t/name
let $txt := $t/text
return file:write($name/string(), $txt/string())