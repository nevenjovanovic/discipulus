(: tokenize a CroALa CTS URN on colons :)
declare function local:tokenize-cts($cts) {
  tokenize($cts, ":")
};

(: tokenize / break down an edition name on dots :)
declare function local:tokenize-edition($edition){
  tokenize($edition, "\.")
};

declare variable $root := "/home/neven/Repos/ogl-moji-2017/canonical-latinLit/data/";
declare variable $urn := ("urn:cts:latinLit:phi1017.phi015", "urn:cts:latinLit:phi0134.phi006", "urn:cts:latinLit:phi0660.phi001", "urn:cts:latinLit:phi0893.phi001");

for $u in $urn
let $dir := $root || string-join(local:tokenize-edition(local:tokenize-cts($u)[4]), "/")
return for $f in file:list($dir)
let $docpath := $dir || "/" || $f
where matches($f, "-lat")
(: return db:create(replace($f, ".xml", ""), $docpath, (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() }) :)
return replace($f, ".xml", "")