declare function local:tokenize_text_nodes($nodes){
let $token_sequence := lower-case(string-join($nodes, ' '))
return tokenize($token_sequence, "[\W~]+")
};

(: get frequency list from corpus :)
let $text_no_notes := for $t in collection("psl-corpus-2017")/body
let $urn := $t/@xml:base
let $nodes := $t//text()[not(ancestor::*:note)]
let $textlist := local:tokenize_text_nodes($nodes)
return ( $urn/string() , count($textlist) )
return $text_no_notes
