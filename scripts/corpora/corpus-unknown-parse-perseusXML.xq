declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=lat&amp;engine=morpheuslat&amp;word=" || $w
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
let $path := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/psl-wordlist.txt.unk"
let $file := file:read-text($path)
let $words :=
for $f in tokenize($file, "\n")
return $f
for $w in $words
where not(matches($w, "\*"))
return local:pers_morph($w)//*:RDF