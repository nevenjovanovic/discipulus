let $string := "[Cc]oma.*"
let $result := element csv {
for $s in collection("psl-sent")//w[matches(., $string)]/..
let $w := $s/w[matches(., $string)]
let $cloze := "{{c1::" || lower-case($w) || "::kosa}}"
let $tag := "coma"
let $text := replace(lower-case(normalize-space($s)), " (\W)", "$1")
return element s {
  element b { replace($text, lower-case($w), $cloze) },
  element a { $tag } 
} }
return csv:serialize($result, map { 'separator': 'tab' })