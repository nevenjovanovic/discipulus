(: count words in sentences, return stats :)
for $s in collection("psl-sent")//s
let $count := count($s/w)
(:  where $count > 50
return normalize-space(data($s)) :)
group by $count
order by $count descending
return element tr {
  element td { $count },
  element td { count($s)}
}