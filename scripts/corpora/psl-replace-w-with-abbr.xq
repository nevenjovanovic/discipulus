for $d in collection("psl-sent")//w[text()="Ct" and following-sibling::pc[1][text()="."]]
let $newd := element abbr {
  $d/text() ,
  element am { data($d/following-sibling::*[1]) }
}
where count($d/../w) = 1
return replace node $d with $newd