declare function local:format-word($word){
  lower-case($word)
};
declare function local:word-lemlat-occur($word){
  let $l := db:open("psl-lemlat")//*:ab/*:w[*:orig=$word]
let $occur := db:open("psl-sent")//*:s[*:w[local:format-word(string())=$word]]
return element result { $l ,
for $o in $occur return element s { normalize-space($o) }  }
};
let $word := "freta"
return local:word-lemlat-occur($word)