(: Analyse lemmata by LEMLAT id and freq :)
for $w in collection("psl-sent")//*:w[@lemmaRef]
let $l := $w/@lemmaRef
group by $l
order by count($w) descending
where count($w) > 10
return element l {
  $l , count($w)
}