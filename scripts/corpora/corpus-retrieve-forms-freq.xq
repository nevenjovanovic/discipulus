for $w in collection("psl-sent")//*:s/*:w[not(@lemmaRef)]
let $form := lower-case($w)
group by $form
(: where count($w) > 20 :)
(: order by count($w) descending :)
order by $form
return element w { $form , count($w) }