let $values := (100, 0)
let $w2 := for $i in collection("psl-lemlat")//*:w
where count($i/*:interpGrp) = 2 and not($i/*:interpGrp/@cert=$values)
return $i
for $w in $w2 
let $ana1 := $w/*:interpGrp[1]/*:interp/@ana
let $ana2 := $w/*:interpGrp[2]/*:interp/@ana
where $ana1 = $ana2 and matches($ana2[1]/../@n, "lemlat:[0-9]")
return $w/*:orig