declare function local:format-word($word){
  lower-case($word)
};
declare %updating function local:word-lemlat-update($word){
  let $l := db:open("psl-lemlat")//*:ab/*:w[*:orig=$word]
  let $lemmaRef := attribute lemmaRef { $l/*:interpGrp[@cert="100"]/*:interp/@n }
for $occur in db:open("psl-sent")//*:s/*:w[local:format-word(string())=$word]
return insert node $lemmaRef into $occur
};
let $words := <w>
<orig>a</orig>
<orig>acceptus</orig>
<orig>accipiter</orig>
<orig>acrem</orig>
<orig>acrior</orig>
<orig>acuta</orig>
<orig>acuto</orig>
<orig>adventus</orig>
<orig>aequalem</orig>
<orig>aequalis</orig>
<orig>aeternum</orig>
<orig>africo</orig>
<orig>africum</orig>
<orig>ager</orig>
<orig>agrestis</orig>
<orig>agros</orig>
<orig>albos</orig>
<orig>albus</orig>
<orig>amabilem</orig>
<orig>amissi</orig>
<orig>amore</orig>
<orig>amorem</orig>
<orig>amores</orig>
<orig>amori</orig>
<orig>amoribus</orig>
<orig>amplissima</orig>
<orig>amplius</orig>
<orig>angues</orig>
<orig>angulum</orig>
<orig>animae</orig>
<orig>animam</orig>
<orig>animi</orig>
<orig>animos</orig>
<orig>animus</orig>
<orig>antiquus</orig>
<orig>aper</orig>
<orig>apollo</orig>
<orig>arbiter</orig>
<orig>arbitrum</orig>
<orig>arbor</orig>
<orig>arborem</orig>
<orig>arboribus</orig>
<orig>arca</orig>
<orig>aristoteles</orig>
<orig>asper</orig>
<orig>asperum</orig>
<orig>astutus</orig>
<orig>atro</orig>
<orig>atrox</orig>
<orig>atrum</orig>
<orig>auctore</orig>
<orig>audax</orig>
<orig>augur</orig>
<orig>aurae</orig>
<orig>aurarum</orig>
<orig>auspice</orig>
<orig>avarior</orig>
<orig>bacchum</orig>
<orig>bacchus</orig>
<orig>barbam</orig>
<orig>benigna</orig>
<orig>benigne</orig>
<orig>benignius</orig>
<orig>benigno</orig>
<orig>blande</orig>
<orig>blandius</orig>
<orig>blandos</orig>
<orig>blandum</orig>
<orig>bone</orig>
<orig>bonos</orig>
<orig>bonus</orig>
<orig>caelestes</orig>
<orig>caeruleus</orig>
<orig>caesar</orig>
<orig>caesarem</orig>
<orig>callide</orig>
<orig>callidum</orig>
<orig>callidus</orig>
<orig>caloris</orig>
<orig>camillum</orig>
<orig>camus</orig>
<orig>candide</orig>
<orig>candidos</orig>
<orig>candidus</orig>
<orig>canus</orig>
<orig>capellae</orig>
<orig>caput</orig>
<orig>carbo</orig>
<orig>carior</orig>
<orig>carissimorum</orig>
<orig>caritatis</orig>
<orig>carius</orig>
<orig>carus</orig>
<orig>casus</orig>
<orig>certae</orig>
<orig>certe</orig>
<orig>certon</orig>
<orig>certos</orig>
<orig>certus</orig>
<orig>cerva</orig>
<orig>chloe</orig>
<orig>citus</orig>
<orig>civitate</orig>
<orig>claram</orig>
<orig>classe</orig>
<orig>cleanthes</orig>
<orig>clemens</orig>
<orig>clementem</orig>
<orig>cliens</orig>
<orig>clio</orig>
<orig>cohors</orig>
<orig>cohorsque</orig>
<orig>colaphis</orig>
<orig>colaphos</orig>
<orig>colles</orig>
<orig>collibus</orig>
<orig>colonus</orig>
<orig>columba</orig>
<orig>columnam</orig>
<orig>communem</orig>
<orig>communes</orig>
<orig>condicio</orig>
<orig>condicionibus</orig>
<orig>condicionis</orig>
<orig>conscientiam</orig>
<orig>consuetudinem</orig>
<orig>consuetudini</orig>
<orig>consuetudo</orig>
<orig>contra</orig>
<orig>contumeliam</orig>
<orig>credula</orig>
<orig>credulus</orig>
<orig>cressa</orig>
<orig>crines</orig>
<orig>crinibus</orig>
<orig>cupide</orig>
<orig>cupidinum</orig>
<orig>cupidum</orig>
<orig>cupressi</orig>
<orig>curae</orig>
<orig>curam</orig>
<orig>custode</orig>
<orig>custodem</orig>
<orig>custodes</orig>
<orig>custos</orig>
<orig>daedalus</orig>
<orig>dea</orig>
<orig>deae</orig>
<orig>deam</orig>
<orig>decem</orig>
<orig>decorae</orig>
<orig>decoram</orig>
<orig>dextro</orig>
<orig>diei</orig>
<orig>dierum</orig>
<orig>dies</orig>
<orig>digne</orig>
<orig>dignos</orig>
<orig>dignum</orig>
<orig>dignus</orig>
<orig>doctos</orig>
<orig>doctus</orig>
<orig>dolore</orig>
<orig>dolores</orig>
<orig>doloribus</orig>
<orig>domibus</orig>
<orig>dominos</orig>
<orig>dominus</orig>
<orig>domni</orig>
<orig>duros</orig>
<orig>durum</orig>
<orig>durus</orig>
<orig>ebrietati</orig>
<orig>eburnus</orig>
<orig>efficaces</orig>
<orig>efficax</orig>
<orig>eois</orig>
<orig>erroribus</orig>
<orig>euro</orig>
<orig>eurus</orig>
<orig>eurusque</orig>
<orig>euterpe</orig>
<orig>fabriciumque</orig>
<orig>facete</orig>
<orig>facilem</orig>
<orig>faciles</orig>
<orig>facili</orig>
<orig>facilis</orig>
<orig>facunde</orig>
<orig>fallaci</orig>
<orig>fallacis</orig>
<orig>fama</orig>
<orig>famam</orig>
<orig>familia</orig>
<orig>familiae</orig>
<orig>familiam</orig>
<orig>fatales</orig>
<orig>fauno</orig>
<orig>faunus</orig>
<orig>febrium</orig>
<orig>felicitatem</orig>
<orig>ferox</orig>
<orig>festivissime</orig>
<orig>fida</orig>
<orig>filii</orig>
<orig>filio</orig>
<orig>flammam</orig>
<orig>flavum</orig>
<orig>fluctibus</orig>
<orig>foco</orig>
<orig>focus</orig>
<orig>fraude</orig>
<orig>frequens</orig>
<orig>frigus</orig>
<orig>furorem</orig>
<orig>fusce</orig>
<orig>futuri</orig>
<orig>futurum</orig>
<orig>garrulus</orig>
<orig>generosa</orig>
<orig>generosius</orig>
<orig>gradu</orig>
<orig>grata</orig>
<orig>grataque</orig>
<orig>grato</orig>
<orig>gratum</orig>
<orig>gratus</orig>
<orig>habili</orig>
<orig>homine</orig>
<orig>hominem</orig>
<orig>homines</orig>
<orig>homini</orig>
<orig>hominibus</orig>
<orig>hominis</orig>
<orig>hominum</orig>
<orig>homo</orig>
<orig>honestiore</orig>
<orig>honestius</orig>
<orig>hora</orig>
<orig>horam</orig>
<orig>horarum</orig>
<orig>horas</orig>
<orig>hortulis</orig>
<orig>hydaspes</orig>
<orig>ianua</orig>
<orig>ianuam</orig>
<orig>ignem</orig>
<orig>ignibus</orig>
<orig>imis</orig>
<orig>impetus</orig>
<orig>impurum</orig>
<orig>impurus</orig>
<orig>ingens</orig>
<orig>iniurioso</orig>
<orig>iocus</orig>
<orig>irae</orig>
<orig>iubae</orig>
<orig>iucundam</orig>
<orig>iucundissima</orig>
<orig>iucundissimum</orig>
<orig>iucundius</orig>
<orig>iucundos</orig>
<orig>iudice</orig>
<orig>iustitiae</orig>
<orig>iuvenem</orig>
<orig>iuveni</orig>
<orig>iuvenibus</orig>
<orig>iuvenis</orig>
<orig>lacertae</orig>
<orig>laetam</orig>
<orig>laetitia</orig>
<orig>laetitiae</orig>
<orig>laetius</orig>
<orig>lamiae</orig>
<orig>large</orig>
<orig>lascivus</orig>
<orig>latronem</orig>
<orig>laudibus</orig>
<orig>lauros</orig>
<orig>lentus</orig>
<orig>lepidus</orig>
<orig>leuconoe</orig>
<orig>lex</orig>
<orig>liberali</orig>
<orig>liberalis</orig>
<orig>liberalitate</orig>
<orig>libertatem</orig>
<orig>limen</orig>
<orig>liquidam</orig>
<orig>liquidas</orig>
<orig>litteris</orig>
<orig>livor</orig>
<orig>longe</orig>
<orig>longos</orig>
<orig>longum</orig>
<orig>lucem</orig>
<orig>lucus</orig>
<orig>lunam</orig>
<orig>lupos</orig>
<orig>lupum</orig>
<orig>lupus</orig>
<orig>luxuriosum</orig>
<orig>lyrae</orig>
<orig>maculam</orig>
<orig>magistrum</orig>
<orig>magnos</orig>
<orig>magnus</orig>
<orig>magnusque</orig>
<orig>maiae</orig>
<orig>maiestatem</orig>
<orig>maior</orig>
<orig>maiorem</orig>
<orig>maiores</orig>
<orig>maioribus</orig>
<orig>malvae</orig>
<orig>mammam</orig>
<orig>mater</orig>
<orig>maxima</orig>
<orig>maximam</orig>
<orig>maxime</orig>
<orig>maximo</orig>
<orig>maximum</orig>
<orig>maximus</orig>
<orig>melior</orig>
<orig>meliore</orig>
<orig>melpomene</orig>
<orig>memini</orig>
<orig>memor</orig>
<orig>mens</orig>
<orig>mensorem</orig>
<orig>meos</orig>
<orig>mercuri</orig>
<orig>mercurius</orig>
<orig>mercuriusque</orig>
<orig>metu</orig>
<orig>metum</orig>
<orig>metus</orig>
<orig>meus</orig>
<orig>mimos</orig>
<orig>minoribus</orig>
<orig>miseria</orig>
<orig>miseriam</orig>
<orig>miseriarum</orig>
<orig>miseriis</orig>
<orig>modesta</orig>
<orig>modeste</orig>
<orig>monte</orig>
<orig>montibus</orig>
<orig>monumenta</orig>
<orig>mordaces</orig>
<orig>mors</orig>
<orig>morte</orig>
<orig>mortem</orig>
<orig>morti</orig>
<orig>natalemque</orig>
<orig>natura</orig>
<orig>naturae</orig>
<orig>naturam</orig>
<orig>navita</orig>
<orig>necessitas</orig>
<orig>necessitate</orig>
<orig>necessitates</orig>
<orig>necessitatis</orig>
<orig>nemo</orig>
<orig>nepotes</orig>
<orig>nepoti</orig>
<orig>niger</orig>
<orig>nitidis</orig>
<orig>nitidum</orig>
<orig>nivalis</orig>
<orig>noctem</orig>
<orig>noctes</orig>
<orig>noctis</orig>
<orig>noster</orig>
<orig>nosti</orig>
<orig>nostris</orig>
<orig>nostro</orig>
<orig>nostrorum</orig>
<orig>novos</orig>
<orig>novum</orig>
<orig>novus</orig>
<orig>nox</orig>
<orig>nucis</orig>
<orig>nudis</orig>
<orig>nudum</orig>
<orig>nudus</orig>
<orig>numina</orig>
<orig>nutrix</orig>
<orig>occasionem</orig>
<orig>oceano</orig>
<orig>oceanoque</orig>
<orig>ocior</orig>
<orig>olivae</orig>
<orig>olivam</orig>
<orig>operae</orig>
<orig>operam</orig>
<orig>oppidum</orig>
<orig>optima</orig>
<orig>optimam</orig>
<orig>optime</orig>
<orig>orco</orig>
<orig>pacuvius</orig>
<orig>palaestrae</orig>
<orig>parthus</orig>
<orig>pastor</orig>
<orig>pastoremque</orig>
<orig>patera</orig>
<orig>patinas</orig>
<orig>pecuniam</orig>
<orig>perpetuos</orig>
<orig>perpetuus</orig>
<orig>pertinaci</orig>
<orig>pestemque</orig>
<orig>pestes</orig>
<orig>pharetra</orig>
<orig>phidias</orig>
<orig>philosophia</orig>
<orig>philosophiae</orig>
<orig>philosophiam</orig>
<orig>philyra</orig>
<orig>pholoe</orig>
<orig>pie</orig>
<orig>pietas</orig>
<orig>plance</orig>
<orig>plaustro</orig>
<orig>pompei</orig>
<orig>potestas</orig>
<orig>potestate</orig>
<orig>primam</orig>
<orig>primarum</orig>
<orig>primus</orig>
<orig>prisci</orig>
<orig>probissime</orig>
<orig>prudens</orig>
<orig>pudor</orig>
<orig>pudore</orig>
<orig>pudoris</orig>
<orig>pugnae</orig>
<orig>pulchre</orig>
<orig>pupillus</orig>
<orig>purpurei</orig>
<orig>purpureos</orig>
<orig>regulum</orig>
<orig>remo</orig>
<orig>ripa</orig>
<orig>ripae</orig>
<orig>rotundum</orig>
<orig>rusticus</orig>
<orig>sacer</orig>
<orig>sacerdotum</orig>
<orig>sacerque</orig>
<orig>sacrae</orig>
<orig>sacram</orig>
<orig>sagittis</orig>
<orig>salaminius</orig>
<orig>saliaribus</orig>
<orig>sannio</orig>
<orig>sapientiae</orig>
<orig>scauros</orig>
<orig>senatu</orig>
<orig>senatum</orig>
<orig>seneca</orig>
<orig>senectutis</orig>
<orig>sericas</orig>
<orig>sermo</orig>
<orig>severi</orig>
<orig>severus</orig>
<orig>sin</orig>
<orig>sollicitudine</orig>
<orig>sollicitudinem</orig>
<orig>sollicitudines</orig>
<orig>sollicitudo</orig>
<orig>sopor</orig>
<orig>spe</orig>
<orig>spem</orig>
<orig>spes</orig>
<orig>stola</orig>
<orig>sulla</orig>
<orig>superba</orig>
<orig>superbae</orig>
<orig>superbia</orig>
<orig>superbo</orig>
<orig>superbos</orig>
<orig>synapothnescontes</orig>
<orig>syre</orig>
<orig>syrtis</orig>
<orig>syrus</orig>
<orig>taurus</orig>
<orig>tellurem</orig>
<orig>tellus</orig>
<orig>terminos</orig>
<orig>terra</orig>
<orig>terrae</orig>
<orig>terram</orig>
<orig>terrarum</orig>
<orig>terras</orig>
<orig>terris</orig>
<orig>tertium</orig>
<orig>thessalosque</orig>
<orig>tisiphoneque</orig>
<orig>toga</orig>
<orig>togae</orig>
<orig>togam</orig>
<orig>tragoedias</orig>
<orig>tranquillitati</orig>
<orig>truculentus</orig>
<orig>turbae</orig>
<orig>turbam</orig>
<orig>ulmo</orig>
<orig>ultor</orig>
<orig>ultorem</orig>
<orig>unice</orig>
<orig>varii</orig>
<orig>vaticani</orig>
<orig>verax</orig>
<orig>verecundiam</orig>
<orig>veros</orig>
<orig>viae</orig>
<orig>viam</orig>
<orig>victor</orig>
<orig>victorem</orig>
<orig>victoriam</orig>
<orig>virga</orig>
<orig>virgaque</orig>
<orig>virgine</orig>
<orig>virginem</orig>
<orig>virgines</orig>
<orig>virginum</orig>
<orig>virgo</orig>
<orig>virtus</orig>
<orig>virtute</orig>
<orig>virtutem</orig>
<orig>virtutes</orig>
<orig>virtutibus</orig>
<orig>vite</orig>
<orig>vitula</orig>
<orig>vituli</orig>
<orig>volcanus</orig>
<orig>zenonem</orig>
</w>
for $word in ("suis")
return local:word-lemlat-update($word)