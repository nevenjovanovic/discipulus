(: with a selection of text segments (corrected) in data directory, create a db :)
let $psl := "/home/neven/Repos/discipulus/d/xmlsent-lemmatized"
return db:create("psl-sent", $psl, (), map { 'updindex': true(), 'autooptimize': true(), 'ftindex': true(), 'intparse' : true(), 'chop' : false() })