for $w in collection("psl-sent")//s/w[not(@lemmaRef)]
let $f := lower-case($w/string())
group by $f
return $f