declare function local:mark_pc($s){
  for $m in analyze-string($s, "[^\w\s]+")/*
  return if ($m/name()="fn:match") then element pc {$m/string() }
  else $m/string()
};
declare variable $xml_file := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/xmlsentences/phi1017.phi015.perseus-lat2.1.12.3.xml";

let $dir := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/xmlsentences/"
let $files := file:list($dir)
for $f in $files return
copy $xml := fetch:xml($dir || $f)
modify ( for $s in $xml//s
let $n := $s/@n
return replace node $s with
element s {
  $n,
  local:mark_pc($s) 
}
)
return file:write($dir || $f , $xml)