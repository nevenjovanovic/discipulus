let $lu := collection("psl-sent")//w[not(@lemmaRef) and string()="procul"]
let $newlu := element w { 
attribute  lemmaRef { "lemlat:p3684" } , "procul" }
return replace node $lu with $newlu
(: return $lu/.. :)