let $lemma := "lemlat:l0691"
let $result := element csv {
for $s in collection("psl-sent")//w[@lemmaRef=$lemma]/..
let $w := $s/w[@lemmaRef=$lemma]
let $cloze := "{{c1::" || lower-case($w[1]) || "::hoće se}}"
let $tag := "libet"
let $text := replace(lower-case(normalize-space($s)), " (\W)", "$1")
return element s {
  element b { replace($text, lower-case($w[1]), $cloze) },
  element a { $tag } 
} }
return csv:serialize($result, map { 'separator': 'tab' })