declare variable $urn := ("phi1017.phi015", "phi0134.phi006", "phi0660.phi001", "phi0893.phi001");
declare function local:prepare_text($s){
  replace(
  replace(lower-case(normalize-space($s)), " (\W)", "$1"),
  '"', '')
};
for $u in $urn
for $f in collection("psl-sent")/div[starts-with(@xml:id, $u)]/s
where count($f/w) = 1 (: > 5 and $f/w[@lemmaRef="lemlat:i3211"] :)
return local:prepare_text($f)