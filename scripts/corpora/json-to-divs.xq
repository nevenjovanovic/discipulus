(: take JSON of a sentence-tokenized text, transform into sequence of divs, using file names for identifiers :)

declare function local:json_tei_fragment($source){
  for $p in json-to-xml($source)//*:array[*:string]
return
  for $s in $p/*:string
  let $ns := count($s/preceding-sibling::*:string) + 1
  return element s { 
  attribute n { "s" || $ns },
  normalize-space($s/string()) } 
};
(: read from a json files directory :)
let $dir := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/json"
let $files := file:list($dir)
for $f in $files 
let $source := file:read-text($dir || "/" || $f)
let $name := file:name($f)
let $node_id := replace($name, "\.txt\.json", "")
let $xml_name := $node_id || ".xml"
let $xml_path := "/home/neven/Repos/discipulus/d/discipulus/psl-corpus/xmlsentences/"
let $text := element div {
  attribute xml:id { $node_id },
  local:json_tei_fragment($source)
 }

return file:write($xml_path || $xml_name , $text )