declare function local:tokenize_text_nodes($nodes){
let $token_sequence := lower-case(string-join($nodes, ' '))
return tokenize($token_sequence, "[\W~]+")
};

(: get frequency list from corpus :)
let $frequencies :=
let $text_no_notes := for $t in collection("psl-corpus-2017")//text()[not(ancestor::*:note)]
return $t
let $textlist := local:tokenize_text_nodes($text_no_notes)

for $l in distinct-values($textlist)
order by $l
return element f {
  attribute freq { count(index-of($textlist, $l)) },
  $l }
return element wl {
  $frequencies
}