for $w in collection("psl-sent")//w[@lemmaRef]
let $l := $w/@lemmaRef
group by $l
let $lemma := distinct-values(collection("psl-lemlat")//*:interp[@n=$l]/@ana)
order by count($w) descending
return element r {
  $l ,
  $lemma ,
  count($w)
}