declare function local:down_tree($root){
  let $id := $root/@id
  let $relations := ("OBJ", "ATR")
  let $down_t := $root/../*:word[@head=$id and @relation=$relations]
  return if ($down_t[@head]) then ( 
  for $t in $down_t return ( $t/@form/string() , local:down_tree($t) ) )
  else $down_t/@form/string()
};
let $phr :=
element sve {
for $prep in db:open("pdl_tb_lat","phi0631.phi001.perseus-lat1.tb.xml")/treebank/body/sentence/word[@relation="PRED"]
return element phr { $prep/@form/string() , local:down_tree($prep) }
}
for $p in $phr/phr
let $toks := tokenize($p, ' ')
where $toks[2]
order by $p
return element phr { for $w in $toks return element w { $w } }