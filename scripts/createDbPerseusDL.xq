(: Pull treebanks from PerseusDL Github repo :)
let $path := '/home/neven/rad/ogl/treebank_data/v2.1/Latin/texts' 
(: return db:create("lat_pdl_tb", $path, (), map { 'intparse' : true() }) :)
let $files := file:children($path)
return db:create("lat_tb", $path, (), map { 'intparse' : true() })