let $word := "principium"
for $s in db:open("lat_nt_tb","latin-nt.xml")/proiel/source/div/sentence[token/@lemma=$word]
return element s { $s/token/@form/string() }