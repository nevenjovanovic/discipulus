declare option output:method "xml";
(: declare option output:cdata-section-elements "text2"; :)

declare function local:shuffle_cards($cards){
    for $card in $cards/*
    order by random:integer()
    return
        $card
};


declare function local:getvalues($values){
  element c { 
  for $dv in distinct-values($values)
  return element w { $dv } }
};

declare function local:getsimilarwords($word, $pos, $meaning){
  let $length := string-length($word)
  for $w in db:open("greeklatincore","latin_core_list_2.xml")/csv/record[not(Lemma=$word) and Lemma[string-length(string())>=$length] and not(DefHR=$meaning) and Part-of-Speech=$pos]
  return $w/DefHR
};

declare function local:createchoices($q){
  let $entry := db:open("greeklatincore","latin_core_list_2.xml")/csv/record[Lemma=$q]
  let $meaning := element w { $entry/DefHR/string() }
  let $pos := $entry/Part-of-Speech/string()
  let $meaning_hr := $entry/DefHR/string()
let $cards := for $w in local:shuffle_cards(
  local:getvalues(
  local:getsimilarwords($q, $pos , $meaning_hr)
))[position() < 6]
return $w
return element choices { $meaning , $cards }
};

declare function local:multichoice($word, $meaning , $dict) { 
let $wdisplay := lower-case(replace($word, "V", "U"))
return
  <question type="multichoice">
    <name>
      <text>{$word}{random:uuid()}</text>
    </name>
    <questiontext format="plain_text">
      <text>{$wdisplay}</text>
    </questiontext>
    <generalfeedback format="plain_text">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
    <single>true</single>
    <shuffleanswers>true</shuffleanswers>
    <answernumbering>abc</answernumbering>
    <correctfeedback format="plain_text">
      <text>{$dict}</text>
    </correctfeedback>
    <partiallycorrectfeedback format="plain_text">
      <text></text>
    </partiallycorrectfeedback>
    <incorrectfeedback format="plain_text">
      <text>Vaš odgovor nije točan.</text>
    </incorrectfeedback>
    <shownumcorrect/>
    <answer fraction="100" format="plain_text">
      <text>{$meaning/w[1]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
    <answer fraction="0" format="plain_text">
      <text>{$meaning/w[2]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
    <answer fraction="0" format="plain_text">
      <text>{$meaning/w[3]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
    <answer fraction="0" format="plain_text">
      <text>{$meaning/w[4]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
    <answer fraction="0" format="plain_text">
      <text>{$meaning/w[5]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
    <answer fraction="0" format="plain_text">
      <text>{$meaning/w[6]/string()}</text>
      <feedback format="plain_text">
        <text></text>
      </feedback>
    </answer>
  </question>
};
element quiz {
  let $questions :=
for $lemma in db:open("greeklatincore","latin_core_list_2.xml")/csv/record[not(DefHR="TBA")]
let $word :=$lemma/Lemma/string()
let $dict := substring-before(substring-after($lemma/Headword/string(), '"&gt;'), '&lt;/a')
let $meaning := local:createchoices($word)
(: order by xs:integer($lemma/Frequency-Rank) :)
order by random:integer()
return local:multichoice($word, $meaning, $dict)
return $questions[position()<101]
}