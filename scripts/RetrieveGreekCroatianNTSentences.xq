declare function local:table($rows){
  element table {
    attribute class { "table"},
  element thead {
    element tr {
      element td { "Greek"},
      element td {"Croatian"}
    }
  },
  element tbody {
    $rows
  }
}
};


let $pairs :=
for $s in db:open("nt_tb","greek-nt.xml")/proiel/source/div/sentence
let $id := $s/@id/string()
let $text := $s/token/@form/string()
let $ref := "grc:" || $id
let $hrv := db:open("nt_tb","croatian-nt.xml")/*:TEI/*:text/*:body/*:div/*:p/*:s[@corresp=$ref]
where $hrv
let $sequence := element tr {
  element td { string-join($text, ' ') } , 
  element td { string-join(for $w in $hrv/*:w return data($w), ' ')  }
}
return $sequence
return local:table( $pairs )