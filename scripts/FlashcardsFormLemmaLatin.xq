declare option output:method 'text';
declare function local:compare_forms_latin($col){
 collection($col)//*:sentence/*:word[not(@lemma="punc1") and not(lower-case(@form)=replace(@lemma, '[0-9]$', ''))]
};

declare function local:shuffle_cards($cards){
    for $card in $cards/*
    order by random:integer()
    return
        $card
};

(: shuffle in random order :)
(: what to do when the same form appears twice :)

let $dialogs := 
element dialogs {
for $w in local:compare_forms_latin("tb-simple-1")
return element _ {
  attribute type {"object"},
  element tips {},
  element text { $w/@form/string() },
  element answer { replace($w/@lemma/string(), '[0-9]$', '')}
}
}
let $json := element json
{ attribute type {"object"},
element dialogs { 
attribute type {"array"},
local:shuffle_cards($dialogs) }
}
return json:serialize($json)