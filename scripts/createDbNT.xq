(: Pull from NT xml files from dir in local repo :)
let $path := file:parent(static-base-uri()) 
let $files := ("greek-nt.xml", "latin-nt.xml", "marianus.xml", "croatian-nt.xml")
let $fullpath := replace($path, '/scripts/', '/biblije-tb/')
return db:create("nt_tb", $fullpath, (), map { 'intparse' : true() })