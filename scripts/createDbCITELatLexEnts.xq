let $path := replace(file:parent(static-base-uri()), '/scripts/', '/vocabs/cite_latlexents') 
return db:create("lat_tb", $path, (), map { 'ftindex': true(), 'intparse' : true() })