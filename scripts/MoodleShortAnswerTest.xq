declare option output:method "xml";
(: declare option output:cdata-section-elements "text2"; :)

declare function local:shortanswer($word, $meaning , $dict) { 
let $question := "Navedite hrvatsko značenje latinske riječi " || $word || ": ___________."
return
  <question type="shortanswer">
    <name>
      <text>{ $word }</text>
    </name>
    <questiontext format="html">
      <text>{$question}</text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
    <usecase>0</usecase>
    <answer fraction="100" format="moodle_auto_format">
      <text> { $meaning } </text>
      <feedback format="html">
        <text>{ $dict }</text>
      </feedback>
    </answer>
    <answer fraction="0" format="moodle_auto_format">
      <text>*</text>
      <feedback format="html">
        <text>"Nažalost, to nije točno značenje!"</text>
      </feedback>
    </answer>
  </question>
};
element quiz {
for $lemma in db:open("greeklatincore","latin_core_list_2.xml")/csv/record[not(DefHR="TBA")]
let $word := lower-case(replace($lemma/Lemma/string(), "V", "U"))
let $dict := substring-before(substring-after($lemma/Headword/string(), '"&gt;'), '&lt;/a')
let $meaning := $lemma/DefHR/string()
return local:shortanswer($word, $meaning, $dict)
}