for $w in db:open("lat_simpl_tok","simpl.xml")/body/cit/quote/s/w
let $parse := db:open("lat_simpl_parsed","parses-unamb.xml")/body/w[@form=lower-case($w)]
return if ($parse/*:entry) then replace node $w with element w { 
$parse/*:entry/@uri ,
attribute lemma {$parse/*:entry/*:dict/*:hdwd/string()},
attribute pofs {$parse/*:entry/*:dict/*:pofs/string()},
attribute form { $w/string()},
$w/string()
}
else ()