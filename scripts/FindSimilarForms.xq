declare function local:shuffle_cards($cards){
    for $card in $cards/*
    order by random:integer()
    return
        $card
};
let $word := "mittendos"
let $sub := '^' || substring($word, 1, 4)
let $values :=  
for $w in collection("lat-tb")//*:word[not(@form=$word)]/@form[matches(string(), $sub)]
return $w
let $cards := element c {
let $set := element c { 
for $dv in distinct-values($values)
return element w { $dv } }
return for $w in local:shuffle_cards($set)[position() < 6] return $w, element w { $word }
}
return local:shuffle_cards($cards)