declare function local:prepare_lemma($lemma){
  replace(
    replace(
      upper-case($lemma), 'U', 'V'), 'J', 'I')
};

declare function local:shuffle_cards($cards){
  element c {
    for $card in $cards/*
    order by random:integer()
    return
        $card
      }
};

declare function local:random_positions($iterations, $max){
  for $n in 1 to $iterations return random:integer($max) + 1
};

let $lemma := "autem"
let $lemma_c := element td { local:prepare_lemma($lemma) }
let $records := local:random_positions(5, 996)
let $randoms := for $r in $records
return element td { db:open("greeklatincore","latin_core_list_2.xml")//record[$r]//Lemma/string() }
let $cards := element c { $lemma_c , $randoms[not(r=$lemma_c)] }
let $r := local:shuffle_cards($cards)//td
return element div {
  attribute class { "card-deck"},
  for $cc in $r return
  element div { attribute class {"card"},
  element div { attribute class {"card-block"},
  element p { attribute class {"card-text"},
$cc/text()}
}
}
}