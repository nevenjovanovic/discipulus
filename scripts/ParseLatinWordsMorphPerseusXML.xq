declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=lat&amp;engine=morpheuslat&amp;word=" || $w
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
local:pers_morph("edo")//*:RDF//*:Body