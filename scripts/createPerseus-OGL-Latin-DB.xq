(: create a db of Perseus and OGL Latin texts :)
(: have them in a local cloned repo :)
let $perseus_lat := "/home/neven/Repos/nj-canonical-latinLit/data"
let $csel_lat := "/home/neven/Repos/csel-dev/data"
let $patrolog_lat := "/home/neven/Repos/patrologia_latina-dev/data"
for $c in ($csel_lat)
let $name := substring-before(substring-after($c, "/home/neven/Repos/"), "/data")
return db:create($name, $c, (), map { 'ftindex': true(), 'intparse' : true(), 'chop' : false() })