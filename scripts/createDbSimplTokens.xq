(: Create a simple DB from tokenized sentences :)
let $path := doc('https://bitbucket.org/nevenjovanovic/discipulus/raw/42e3f45259230a30fd810f7d9405502e25a240a2/simplex/simpl.xml') 
return db:create("lat_simpl_tok", $path, (), map { 'intparse' : true() })