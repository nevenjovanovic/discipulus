for $f in db:open("llct_tb","LLCT-with-new-attributes.pml.xml")/*:aldt_treebank/*:aldt_trees//*:LM
let $lemma := $f/@lemma
let $lem_comp := replace($lemma, '[0-9]$', '')
group by $lem_comp
where count($f) > 100
let $dcc := db:open("greeklatincore","latin_core_list_2.xml")/csv/record[Lemma=replace(
  replace(upper-case($lem_comp), "U", "V"), "J", "I")]
order by count($f) descending
return if ($dcc) then $dcc/Lemma else element w { $lem_comp , count($f) }
