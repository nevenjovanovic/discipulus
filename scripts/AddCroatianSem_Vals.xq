let $trls :=
let $file := "/home/neven/rad/discipulus/vocabs/sem_groups_eng_hrv.csv"
let $options := map { 'header': true() }
let $csv := file:read-text($file)
return csv:parse($csv, $options )
for $t in $trls//record
let $eng := $t/*:Engleski
let $hrv := $t/*:Hrvatski
for $w in collection("greeklatincore")//*:node[matches(*:SEMANTIC-GROUP, $eng) or matches(*:Semantic-Group, $eng)]
let $hr_sem := element Semantic-Group {
  attribute xml:lang {"hrv"},
  $hrv/string()
}
return insert node $hr_sem into $w