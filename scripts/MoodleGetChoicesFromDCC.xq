declare function local:shuffle_cards($cards){
    for $card in $cards/*
    order by random:integer()
    return
        $card
};

declare function local:getvalues($values){
  element c { 
  for $dv in distinct-values($values)
  return element w { $dv } }
};

declare function local:getsimilarwords($word){
  let $length := string-length($word)
  for $w in db:open("greeklatincore","latin_core_list_2.xml")/csv/record[not(Lemma=$word) and Lemma[string-length(string())>=$length] and not(DefHR="TBA")]
  return $w/DefHR
};

declare function local:createchoices($q){
  let $meaning := element w { db:open("greeklatincore","latin_core_list_2.xml")/csv/record[Lemma=$q]/DefHR/string() }
let $cards := for $w in local:shuffle_cards(
  local:getvalues(
  local:getsimilarwords($q)
))[position() < 6]
return $w
return element choices { $meaning , $cards }
};
let $q := ("VOX", "PATEO", "QVATTVOR")
for $qq in $q
return local:createchoices($qq)