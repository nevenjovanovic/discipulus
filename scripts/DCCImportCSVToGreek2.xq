let $path := replace(file:parent(static-base-uri()), '/scripts/', '/vocabs/hrv_csv/')
let $f := $path || "greek_core_list_hrv.csv"
let $csv := file:read-text($f)
return file:write("/home/neven/Repos/discipulus/d/discipulus/vocabs/hrv_xml/greek_core_list_2.xml" , csv:parse($csv, map { 'header': true(), 'backslashes': true(), 'separator': 'tab' }) )

