(: Create a DB of unambiguous parses :)
let $path := doc('https://bitbucket.org/nevenjovanovic/discipulus/raw/302d6264cb593d8f89d5f6389ae1f77c4d027c23/simplex/parses-unamb.xml') 
return db:create("lat_simpl_parsed", $path, (), map { 'intparse' : true() })