for $n in //*:node[contains(HEADWORD, "/greek-core/")]
let $lemma := element Lemma {
  replace(
  replace($n/HEADWORD/string(), '.*/greek-core/.*"&gt;', ''),
  '&lt;/a&gt;',
  '')
 }
return insert node $lemma into $n
(: return $lemma :)