for $r in db:open("discip_latlexents","croala-cite-latlexents.xml")/list/record
let $lemma := $r/lemma/string()
let $norm := attribute norm { replace(replace($lemma, 'U', 'V'), 'J', 'I') }
return insert node $norm into $r/lemma