(: db:open("greeklatincore","greek_core_list_2.xml")//record/Lemma/string() :)
let $path := replace(file:parent(static-base-uri()), '/scripts/', '/python/')
let $file := file:read-text($path || "dcclemmata.csv")
let $csv := csv:parse($file, map { 'header': true()})
for $c in $csv//record
let $lu := $c/LemmaUpper
let $l := db:open("greeklatincore","greek_core_list_2.xml")//record[Lemma=$c/Lemma/string()]
return insert node $lu into $l