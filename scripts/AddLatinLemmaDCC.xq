for $n in //*:node[contains(Headword, "http://dcc.dickinson.edu/latin-core/")]
let $lemma := element Lemma {
  upper-case(
  replace(
  replace($n/Headword/string(), '&lt;a href="http://dcc.dickinson.edu/latin-core/', ''),
  '"&gt;.*&lt;/a&gt;',
  '')
) }
return insert node $lemma into $n