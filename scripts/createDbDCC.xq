let $path := replace(file:parent(static-base-uri()), '/scripts/', '/vocabs/hrv_xml') 
return db:create("greeklatincore", $path, (), map { 'ftindex': true(), 'intparse' : true() })