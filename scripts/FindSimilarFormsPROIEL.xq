(: return siblings in random order :)
declare function local:shuffle_cards($cards){
    for $card in $cards/*
    order by random:integer()
    return
        $card
};

(: return distinct forms from tb database, prepare for further selection :)
declare function local:getvalues($values){
  element c { 
  for $dv in distinct-values($values)
  return element w { $dv } }
};

(: get all similar forms, but not the one we're practising on; similar = first 4 characters match :)
(: can be made more sophisticated -- return forms with similar character count! :)
declare function local:getsimilarwords($word){
  let $length := string-length($word)
  let $count := if ($length <= 4) then $length - 1 else 4
  let $sub := substring($word, 1, $count)
  for $w in db:open("proiel-biblia-tb","latin-nt.xml")//*:token[not(@form=$word)]/@form[matches(string(), $sub)]
  return $w
};

declare function local:card_template($w) {
  element div {
    attribute class { "card text-center"},
    element div {
      attribute class { "card-block"},
      element h4 {
        attribute class {"card-title"},
        $w
      }
    }
  }
};

declare function local:card_container($cards){
  element div {
  attribute class {"row"},
  for $c in $cards return element div {
    attribute class {"col-4 mb-3 mt-3"},
    local:card_template($c/string())
  }
}
};

declare function local:json_container ($cards) {
  element json {
    attribute type { "object"},
    element choices {
      attribute type { "array"},
      element _ {
        attribute type { "object"},
        element subContentId { random:uuid() },
        element question { },
        element answers {
          attribute type { "array"},
          for $c in $cards return element _ { 
          "&lt;p&gt;" || $c/string() || "&lt;/p&gt;" }
        }
      }
    },
    <behaviour type="object">
    <timeoutCorrect type="number">2000</timeoutCorrect>
    <timeoutWrong type="number">3000</timeoutWrong>
    <soundEffectsEnabled type="boolean">true</soundEffectsEnabled>
    <enableRetry type="boolean">true</enableRetry>
    <enableSolutionsButton type="boolean">true</enableSolutionsButton>
    <passPercentage type="number">100</passPercentage>
  </behaviour> ,
  <l10n type="object">
    <resultSlideTitle>You got :numcorrect of :maxscore correct</resultSlideTitle>
    <showSolutionButtonLabel>Show solution</showSolutionButtonLabel>
    <retryButtonLabel>Retry</retryButtonLabel>
    <solutionViewTitle>Solution</solutionViewTitle>
    <correctText>Correct!</correctText>
    <incorrectText>Incorrect!</incorrectText>
    <muteButtonLabel>Mute feedback sound</muteButtonLabel>
    <closeButtonLabel>Close</closeButtonLabel>
    <slideOfTotal>Slide :num of :total</slideOfTotal>
  </l10n>
  }
};
let $q := "mare"
let $word := element s { db:open("proiel-biblia-tb","latin-nt.xml")//*:token[@form=$q] }/*:token[1]/@lemma/string()
let $cards := local:shuffle_cards(
  element c {
  for $w in local:shuffle_cards(
    local:getvalues(
      local:getsimilarwords($word)
    )
   )[position() < 6]
  return $w, element w { $word }
}
)
return local:json_container ($cards)