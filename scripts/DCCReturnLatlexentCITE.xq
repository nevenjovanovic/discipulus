for $record in db:open("greeklatincore","latin_core_list_2.xml")//record
let $lemma := $record/Lemma/string()
let $cplemma := db:open("discip_latlexents","croala-cite-latlexents.xml")//record[lemma/@norm=$lemma and not(contains(lemma, 'J'))]
let $cite := if ($cplemma) then element Cite { $cplemma/lemma/@citeurn/string() } else element Cite { "TBA" }
return insert node $cite into $record