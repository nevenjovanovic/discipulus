let $phrases := element phrs {
for $f in  db:open("llct_tb","LLCT-with-new-attributes.pml.xml")/*:aldt_treebank/*:aldt_trees//*:LM[@relation="AuxP" and @pos="preposition"]
let $descendants := $f//*:LM
order by lower-case($f/@form/string()) , lower-case($descendants[1]/@form/string())
return element phr { $f/@form/string() , $descendants/@form/string() }
}
for $ph in $phrases//phr
let $uniq := $ph/string()
group by $uniq
order by count($ph) descending
return element phr { $uniq , count($ph) }