declare function local:hrvmat(){
let $path := replace(file:parent(static-base-uri()), '/scripts/', '/vocabs/hrvmatgrc/')
let $f := $path || "hrvmatgrc.csv"
let $csv := file:read-text($f)
return element csv { csv:parse($csv, map { 'backslashes': true() })//record/entry[1] }
};
for $w in local:hrvmat()//entry/string()
(: where db:open("greeklatincore","greek_core_list_2.xml")//record[Lemma=$w] :)
order by $w
return $w
