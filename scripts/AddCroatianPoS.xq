let $trls :=
let $file := "/home/neven/rad/discipulus/vocabs/morph_eng_hrv.csv"
let $options := map { 'header': true() }
let $csv := file:read-text($file)
return csv:parse($csv, $options )
for $t in $trls//record
let $eng := $t/*:Engleski
let $hrv := $t/*:Hrvatski
for $w in collection("greeklatincore")//*:node[matches(*:PART-OF-SPEECH, $eng) or matches(*:Part-of-Speech, $eng)]
let $hr_sem := element Part-of-Speech {
  attribute xml:lang {"hrv"},
  $hrv/string()
}
return insert node $hr_sem into $w