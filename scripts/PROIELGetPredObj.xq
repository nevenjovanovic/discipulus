declare function local:down_tree($root){
  let $id := $root/@id
  let $relations := ("obj", "xobj", "adj", "obl")
  let $down_t := $root/../*:token[@head-id=$id and @relation=$relations]
  return if ($down_t[@head-id]) then ( 
  for $t in $down_t return ( $t/@form/string() , local:down_tree($t) ) )
  else $down_t/@form/string()
};
let $phr :=
element sve {
for $prep in db:open("proiel-biblia-tb","latin-nt.xml")/proiel/source/div[@alignment-id="41"]/div[@alignment-id="9"]/sentence/token[@relation="pred"]
return element phr { $prep/@citation-part , $prep/@form/string() , local:down_tree($prep) }
}
for $p in $phr/phr
let $toks := tokenize($p, ' ')
where $toks[2]
order by $p
return element phr { $p/@citation-part , for $w in $toks return element w { $w } }