declare function local:down_tree($root){
  let $id := $root/@id
  let $down_t := $root/../*:token[@head-id=$id]
  return if ($down_t[@head-id]) then ( $down_t/@form/string() , local:down_tree($down_t) )
  else $down_t/@form/string()
};
for $phr in
for $prep in db:open("proiel-biblia-tb","latin-nt.xml")/proiel/source/div[@alignment-id="41"]/div[@alignment-id="7"]/sentence/token[@part-of-speech="R-"]
return element phr { $prep/@citation-part , $prep/@form/string() , local:down_tree($prep) }
order by $phr
return element phr { $phr/@citation-part , for $w in tokenize($phr, ' ') return element w { $w } }