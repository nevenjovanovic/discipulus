for $n in db:open("greeklatincore","greek_core_list_2.xml")//*:csv
let $path := replace(file:parent(static-base-uri()), '/scripts/', '/vocabs/hrv_xml/')
let $f := $path || "greek_core_list_2.xml"
return file:write($f , $n)