declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=lat&amp;engine=morpheuslat&amp;word=" || $w
  return 
    fetch:xml($call, map { 'parser' : 'json'})
};
local:pers_morph("edo")