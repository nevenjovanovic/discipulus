declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace rdfs = "http://www.w3.org/2000/01/rdf-schema#";
declare namespace ontolex = "http://www.w3.org/ns/lemon/ontolex#";
declare namespace lila = "http://lila-erc.eu/ontologies/lila/";
let $core := (
for $l in db:open("grclatcore", "latin_core_list_2.xml")//*:record
let $lem := $l/*:Lemma
let $lemsrc := lower-case(replace($lem/string(), "V", "u"))
return $lemsrc )
for $n in 101 to 1000
for $c in $core[$n]
let $lemmaB := collection("lemmaBank")//rdf:Description[rdfs:label[string()=$c]]
return element w { for $ll in $lemmaB return element ref { attribute target { $ll/@rdf:about/string() } ,
$ll/lila:hasPOS , $c } }
