# Discipulus - a learners' digital corpus of Latin and Greek #

Testing the workflow: from annotated sentences to automatically produced second language learning exercises (such as flashcards, multiple choice questions, etc).

### Contents ###

* alignments-simple: simple sentences, aligned with Croatian translations
* tb-simple: simple sentences morphologically annotated and treebanked
* vocabs: [Dickinson Core Vocabularies](http://dcc.dickinson.edu/vocab/core-vocabulary), adapted for Croatian; other basic Croatian vocabularies
* biblije-tb: morphologically annotated editions of New Testament (Greek, Latin, Old Church Slavonic)
* biblije-paralelno: editions of the Bible in several languages (Greek, Latin, Croatian)
* h5p: exercises produced using the [H5P framework](http://h5p.org/)

### How do I get set up? ###

TBA

* Summary of set up
* Configuration

### Contribution guidelines ###

TBA

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Editor: [Neven Jovanović](http://orcid.org/0000-0002-9119-399X)
* Other community or team contact