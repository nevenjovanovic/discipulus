#!/usr/bin/env python # -- coding: utf-8 --
"""
SYNOPSIS

    python iterate-over-files.py filepath

DESCRIPTION

    Input a directory path, iterate over all files in the dir.

EXAMPLES

    python iterate-over-files.py /home/neven/Repos/discipulus/d/discipulus/psl-corpus/plaintext/

EXIT STATUS

    TODO: List exit codes

AUTHOR

    Neven Jovanovic <neven.jovanovic@ffzg.hr>

LICENSE

    This script is in the public domain, free from copyrights or restrictions. CC-licensed.

VERSION

    $0.1$
"""

import sys, os, traceback, optparse
import time
import re
import tokTextFile

global options, args
    
    # TODO: Do something more interesting here...
    # i = sys.argv
for root, dirs, filenames in os.walk('/home/neven/Repos/discipulus/d/discipulus/psl-corpus/plaintext'):
    for f in filenames:
        o = f + ".json"
        tokTextFile.sent(os.path.join(root , f), os.path.join(root, o))    

