#!/usr/bin/env python # -- coding: utf-8 --
"""
SYNOPSIS

    python tokenize-text-file.py filepath outputfile

DESCRIPTION

    Read in a text file, feed it to the Latin tokenizer, output resulting list of sentences as a JSON array.

EXAMPLES

    python tokenize-text-file.py /home/neven/Repos/modruski-temrezah/data/nikolamodr01/croala1394999/nikolamodr01.croala1394999.croala-lat3text.txt modr.json

EXIT STATUS

    TODO: List exit codes

AUTHOR

    Neven Jovanovic <neven.jovanovic@ffzg.hr>

LICENSE

    This script is in the public domain, free from copyrights or restrictions. CC-licensed.

VERSION

    $0.1$
"""

import sys, os, traceback, optparse
import time
import re
#from pexpect import run, spawn
import json
from cltk.tokenize.sentence import TokenizeSentence

def sent (i, o):

    global options, args
    
    # TODO: Do something more interesting here...
    # i = sys.argv[1]
    # o = sys.argv[2]
    sentences = []
    tokenizer = TokenizeSentence('latin')
    with open(i) as txt:
        data=txt.read()
        modr_list = data.splitlines()
    for c in modr_list:
        t = tokenizer.tokenize_sentences(c)
        sentences.append(t)
    sentences_clean = [x for x in sentences if x]
    f = open(o, 'w', encoding='utf8')
    f.write(json.dumps(sentences_clean, ensure_ascii=False))
    f.close()
if __name__ == "__main__":
    sent()

