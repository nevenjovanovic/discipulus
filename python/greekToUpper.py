#!/usr/bin/env python # -- coding: utf-8 --
"""
SYNOPSIS

    python greekToUpper.py

DESCRIPTION

    Read in a Greek word, remove accents and breathings, convert to upper case.

EXAMPLES

    python tokenize-text-file.py /home/neven/Repos/modruski-temrezah/data/nikolamodr01/croala1394999/nikolamodr01.croala1394999.croala-lat3text.txt modr.json

EXIT STATUS

    TODO: List exit codes

AUTHOR

    Neven Jovanovic <neven.jovanovic@ffzg.hr>

LICENSE

    This script is in the public domain, free from copyrights or restrictions. CC-licensed.

VERSION

    $0.1$
"""

import sys, os, traceback, optparse
import time
import re
#from pexpect import run, spawn
# import json
from greek_accentuation.characters import strip_accents , strip_breathing

global options, args
i = sys.argv[1]
with open(i) as txt:
    data=txt.read()
    my_list = data.splitlines()
    # TODO: Do something more interesting here...
for w in my_list:
    print(w + ',' + strip_breathing(strip_accents(w)).upper())
